﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

    public Rigidbody ball;
    public Collider goal;
    public AudioSource audio;

    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        Debug.DrawRay(ball.transform.position, Vector3.down * 20, Color.green);
    }

    void OnTriggerEnter(Collider ball)
    {
        audio.Play();
        Application.LoadLevel(Application.loadedLevel);
    }
}
