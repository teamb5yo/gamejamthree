﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {
    public float speed;
    public float rotationSpeed;
    public float jumpForce;
    public float waterHeight;
    Rigidbody player;
    
	// Use this for initialization
	void Start () {
        player = GetComponent<Rigidbody>();
        waterHeight = 9.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y <= waterHeight)
        {
            player.AddRelativeForce(0, Input.GetAxis("Vertical") * speed, 0);
        }
        else
        {
            player.AddRelativeForce(0, Input.GetAxis("Vertical") * speed/3, 0);
        }
        transform.Rotate(0, 0, Input.GetAxis("Horizontal") * rotationSpeed);

        if (Input.GetKeyDown(KeyCode.Space) && transform.position.y <= waterHeight )
        {
            player.AddRelativeForce(0, 0, jumpForce);
        }
    }
}
